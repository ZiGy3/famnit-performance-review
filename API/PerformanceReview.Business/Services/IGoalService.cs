﻿using System.Collections.Generic;
using PerformanceReview.Business.Models;

namespace PerformanceReview.Business.Services
{
    public interface IGoalService
    {
        Goal GetGoal(int id);
        List<Goal> GetGoalList(int reviewId);
        Goal InsertGoal(Goal goal);
        void UpdateGoal(Goal goal);
        void DeleteGoal(int id);
        void AddEmployeeAssessment(int id, RatingScale rating);
        void AddSuperiorAssessment(int id, RatingScale rating);
    }
}
