﻿using System;
using System.Collections.Generic;
using PerformanceReview.Business.Models;

namespace PerformanceReview.Business.Services
{
    public interface IReviewService
    {
        Review GetReview(int year);
        Review GetEmployeeReview(int year, Guid userGuid);
        void ChangeStatus(int reviewId, ReviewStatus status);
        void AddFinalAssessment(int id, RatingScale rating);
        List<Tuple<Guid, ReviewStatus>> GetEmployeesReviewStatus(int year, Guid userGuid);
    }
}

