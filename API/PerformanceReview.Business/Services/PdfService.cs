﻿using System.Collections.Generic;
using System.Linq;
using DinitPdfExporterConsole;
using iTextSharp.text;
using PerformanceReview.Business.Models;
using PerformanceReview.Business.Repositories;

namespace PerformanceReview.Business.Services
{
    public class PdfService : IPdfService
    {
        private readonly IGoalRepository _goalRepository;

        private readonly IUserService _userService;

        public PdfService(IGoalRepository goalRepository, IUserService userService)
        {
            _goalRepository = goalRepository;
            _userService = userService;
        }

        public byte[] GetReviewByteArray(Review review)
        {
            var reviewId = review.Id ?? default;
            var goals = _goalRepository.GetGoalList(reviewId);

            var finalCommentList = new List<string>();
            review.Comments.ToList().FindAll(comment => comment.Type == 5).ForEach(comment =>
                finalCommentList.Add(
                    $"{comment.Date:dd.MM.yyyy} - {comment.User} ({(CommentType) comment.Type}): {comment.Content}"));
            var finalComments = string.Join("\r\n", finalCommentList);

            string[] paramName =
                {"Goal Title", "Description", "Comments", "Employee Rating", "Superior Rating"};

            var paramData = new string[goals.Count][];

            var i = 0;
            foreach (var goal in goals)
            {
                paramData[i] = goal.ToArray();
                i++;
            }

            var user = _userService.GetUserDetail(review.UserGuid);

            var textBefore = new Text(ReplaceString($"Review for {user.DisplayName} ({review.Year})"), 20, "bold",
                BaseColor.BLACK);

            var textAfter = new Text("");
            if (review.FinalAssessment != null)
            {
                textAfter = new Text(ReplaceString($"{finalComments}\r\n \r\n" +
                                                   "Signature: _________________________" +
                                                   "                                                                                                                                  " +
                                                   $" Final Assessment: {(int) review.FinalAssessment}"), 12, "",
                    BaseColor.BLACK);
            }

            // ReSharper disable once ObjectCreationAsStatement
            // Must be here to work
            new PdfExporter();

            var result = PdfExporter.MakePdfByteArray(null, paramName, paramData, "landscape", textBefore,
                Element.ALIGN_CENTER, textAfter, null);
            return result;
        }

        public static string ReplaceString(string input)
        {
            var output = input
                .Replace('Č', 'C')
                .Replace('č', 'c')
                .Replace('Ć', 'C')
                .Replace('ć', 'c');

            return output;
        }
    }
}