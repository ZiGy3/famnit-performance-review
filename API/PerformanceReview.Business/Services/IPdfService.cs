﻿using PerformanceReview.Business.Models;

namespace PerformanceReview.Business.Services
{
    public interface IPdfService
    {
        byte[] GetReviewByteArray(Review review);
    }
}