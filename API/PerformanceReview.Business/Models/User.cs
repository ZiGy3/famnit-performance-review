﻿using System;
using Novell.Directory.Ldap;

namespace PerformanceReview.Business.Models
{
    public class User
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public Guid Guid { get; set; }
        public Guid? ManagerGuid { get; set; }
        public string DisplayName { get; set; }
        public string Role { get; set; }
        public string Token { get; set; }
        public bool IsSuperior { get; set; }
    }
}
