﻿namespace PerformanceReview.Business.Models
{
    public enum ReviewStatus
    {
        PendingGoalsDefinition = 1,
        CustomGoalsDefined = 2,
        SuperiorGoalsConfirmed = 3,
        EmployeeAssessment = 4,
        SuperiorAssessment = 5,
        ManagementAssessment = 6,
        Closed = 7
    }
}
