﻿using System;

namespace PerformanceReview.Business.Models
{
    public class Comment
    {
        public int? Id { get; set; }
        public int? GoalId { get; set; }
        public int? ReviewId { get; set; }
        public string Content { get; set; }
        public int Type { get; set; }
        public DateTime Date { get; set; }
        public string User { get; set; }
    }
}
