﻿namespace PerformanceReview.Business.Models
{
    public enum GoalType
    {
        Custom = 1, 
        Predefined = 2
    }
}
