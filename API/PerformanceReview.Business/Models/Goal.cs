﻿using System.Collections.Generic;
using PerformanceReview.Business.Services;

namespace PerformanceReview.Business.Models
{
    public class Goal
    {
        public int? Id { get; set; }
        public int? ReviewId { get; set; }
        public GoalType Type { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public RatingScale? EmployeeAssessment { get; set; }
        public RatingScale? SuperiorAssessment { get; set; }
        public List<Comment> Comments { get; set; }

        public string[] ToArray()
        {
            var commentValues = new List<string>();

            Comments.ForEach(comment =>
                commentValues.Add($"{comment.Date:dd.MM.yyyy} - {comment.User} ({(CommentType)comment.Type}): {comment.Content}"));

            var comments = PdfService.ReplaceString(string.Join("\r\n", commentValues));

            string[] array =
            {
                Title,
                Description,
                comments,
                null,
                null
            };

            if (EmployeeAssessment != null)
            {
                if (EmployeeAssessment == RatingScale.NotApplicable)
                {
                    array[3] = "N/A";
                }
                else
                {
                    array[3] = ((int) EmployeeAssessment).ToString();
                }
            }

            if (SuperiorAssessment != null)
            {
                if (SuperiorAssessment == RatingScale.NotApplicable)
                {
                    array[4] = "N/A";
                }
                else
                {
                    array[4] = ((int) SuperiorAssessment).ToString();
                }
            }

            return array;
        }
    }
}