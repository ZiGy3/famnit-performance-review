﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using PerformanceReview.Business.Models;

namespace PerformanceReview.Business.Repositories
{
    public class GoalRepository : IGoalRepository
    {
        private readonly AppDb _db;

        public GoalRepository(AppDb db)
        {
            _db = db;
        }

        public Goal GetGoal(int id)
        {
            return _db.Connection.QueryFirst<Goal>("SELECT * FROM goal WHERE id=@id", new {id});
        }

        public List<Goal> GetGoalList(int reviewId)
        {
            var goalDictionary = new Dictionary<int, Goal>();
            return _db.Connection.Query<Goal, Comment, Goal>(
                @"SELECT G.id, G.title, G.description, G.review_id, G.employee_assessment as employeeAssessment, G.superior_assessment as superiorAssessment, G.type,
	                                                      C.id, C.goal_id, C.review_id, C.content, C.type, C.date, C.date, C.user 
	                                               FROM goal AS G
                                                   LEFT JOIN comment AS C ON G.Id = C.goal_id                              
	                                               WHERE G.review_id = @reviewId ORDER BY G.id, C.id",
                (goal, comment) =>
                {
                    Goal goalEntity = null;
                    if (goal.Id != null)
                    {
                        if (!goalDictionary.TryGetValue(goal.Id.Value, out goalEntity))
                        {
                            goalDictionary.Add(goal.Id.Value, goalEntity = goal);
                            goalEntity.Comments = new List<Comment>();
                        }
                    }

                    if (comment != null)
                    {
                        goalEntity?.Comments.Add(comment);
                    }

                    return goalEntity;
                }, new {reviewId}).Distinct().ToList();
        }

        public Goal InsertGoal(Goal goal)
        {
            var result = _db.Connection.Query<int>(
                @"INSERT INTO goal (review_id, title, description, type, employee_assessment, superior_assessment) 
                                                VALUES (@reviewId, @title, @description, @type, @employeeAssessment, @superiorAssessment);
                                         SELECT LAST_INSERT_ID();",
                new
                {
                    reviewId = goal.ReviewId,
                    title = goal.Title,
                    description = goal.Description,
                    type = goal.Type,
                    employeeAssessment = goal.EmployeeAssessment,
                    superiorAssessment = goal.SuperiorAssessment
                }).Single();
            goal.Id = result;
            return goal;
        }

        public void InsertGoalList(IEnumerable<Goal> goalList)
        {
            using var transaction = _db.Connection.BeginTransaction();
            foreach (var goal in goalList)
            {
                _db.Connection.Execute(
                    @"INSERT INTO goal (review_id, title, description, type, employee_assessment, superior_assessment) 
                                                VALUES (@reviewId, @title, @description, @type, @employeeAssessment, @superiorAssessment)",
                    new
                    {
                        reviewId = goal.ReviewId,
                        title = goal.Title,
                        description = goal.Description,
                        type = goal.Type,
                        employeeAssessment = goal.EmployeeAssessment,
                        superiorAssessment = goal.SuperiorAssessment
                    }, transaction);
            }

            transaction.Commit();
        }

        public void UpdateGoal(Goal goal)
        {
            _db.Connection.Execute(@"UPDATE goal 
                                SET                         
                                   title = @title,
                                   description = @description,
                                   type  = @goalType
                                WHERE id = @id", new
            {
                id = goal.Id,
                title = goal.Title,
                description = goal.Description,
                goalType = goal.Type
            });
        }

        public void DeleteGoal(int id)
        {
            _db.Connection.Execute(@"DELETE FROM goal 
                                WHERE id = @id", new {id});
        }

        public void AddEmployeeAssessment(int id, RatingScale rating)
        {
            _db.Connection.Execute(@"UPDATE goal SET employee_assessment = @rating
                         WHERE id = @id",
                new {id, rating});
        }

        public void AddSuperiorAssessment(int id, RatingScale rating)
        {
            _db.Connection.Execute(@"UPDATE goal SET superior_assessment = @rating
                         WHERE id = @id",
                new {id, rating});
        }
    }
}