﻿using System.Collections.Generic;
using PerformanceReview.Business.Models;

namespace PerformanceReview.Business.Repositories
{
    public interface IGoalRepository
    {
        Goal GetGoal(int id);
        List<Goal> GetGoalList(int reviewId);
        Goal InsertGoal(Goal goal);
        void InsertGoalList(IEnumerable<Goal> goalList);
        void UpdateGoal(Goal goal);
        void DeleteGoal(int id);
        void AddEmployeeAssessment(int id, RatingScale rating);
        void AddSuperiorAssessment(int id, RatingScale rating);
    }
}
