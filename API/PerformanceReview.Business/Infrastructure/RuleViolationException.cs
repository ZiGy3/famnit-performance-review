﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PerformanceReview.Business.Infrastructure
{
    public class RuleViolationException : Exception
    {
        public RuleViolation[] RuleViolations { get; }

        public RuleViolationException(IEnumerable<RuleViolation> ruleViolations) :
            this(ruleViolations.ToArray())
        { }

        private RuleViolationException(params RuleViolation[] ruleViolations) :
            base(ruleViolations.Length > 1 ? "RuleViolationException" : ruleViolations[0].Message)
        {
            RuleViolations = ruleViolations;
        }

    }
}
