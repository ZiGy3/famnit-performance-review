﻿namespace PerformanceReview.Business.Infrastructure
{
    public class ADConfig
    {
        public string Host { get; set; }
        public string Domain { get; set; }
        public string Container { get; set; }
    }

    public class JwtConfig
    {
        public string Secret { get; set; }
    }

    public class ADCredentials
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
