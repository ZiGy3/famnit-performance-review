﻿using Newtonsoft.Json;
using PerformanceReview.Business.Infrastructure;

namespace PerformanceReview.API.Infrastructure
{
    public class ErrorDetails
    {
        public int StatusCode;
        public string Message;

        public RuleViolation[] ValidationErrors;

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
