﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using PerformanceReview.Business.Models;
using PerformanceReview.Business.Services;

namespace PerformanceReview.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]Login login)
        {
            var user = _userService.Authenticate(login);

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(user);
        }

        [HttpGet("employeelist")]
        public IActionResult GetEmployeeList()
        {
            return new JsonResult(_userService.GetEmployeeList(new Guid(HttpContext.User.Identity.Name)));
        }
    }
}