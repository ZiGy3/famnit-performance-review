﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using PerformanceReview.Business.Models;
using PerformanceReview.Business.Services;

namespace PerformanceReview.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GoalController : ControllerBase
    {

        private readonly IGoalService _goalService;

        public GoalController(IGoalService goalService)
        {
            _goalService = goalService;
        }

        [HttpGet("{id}")]
        public IActionResult GetGoal(int id)
        {
            return new JsonResult(_goalService.GetGoal(id));
        }

        [HttpGet("list/{reviewId}")]
        public IActionResult GetGoalList(int reviewId)
        {
            return new JsonResult(_goalService.GetGoalList(reviewId));
        }

        [HttpPost]
        public IActionResult InsertGoal([FromBody] Goal goal)
        {
            return new JsonResult(_goalService.InsertGoal(goal));
        }

        [HttpPut]
        public void UpdateGoal([FromBody] Goal goal)
        {
            _goalService.UpdateGoal(goal);
        }

        [HttpDelete("{id}")]
        public void DeleteGoal(int id)
        {
            _goalService.DeleteGoal(id);
        }

        [HttpPost("employeeassessment")]
        public void AddEmployeeAssessment([FromBody]JsonElement data)
        {
            _goalService.AddEmployeeAssessment(data.GetProperty("goalId").GetInt32(), (RatingScale)data.GetProperty("rating").GetInt32());
        }

        [HttpPost("superiorassessment")]
        public void AddSuperiorAssessment([FromBody]JsonElement data)
        {
            _goalService.AddSuperiorAssessment(data.GetProperty("goalId").GetInt32(), (RatingScale)data.GetProperty("rating").GetInt32());
        }
    }
}