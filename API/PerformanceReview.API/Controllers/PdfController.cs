﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PerformanceReview.Business.Models;
using PerformanceReview.Business.Services;

namespace PerformanceReview.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PdfController : ControllerBase
    {
        private readonly IPdfService _pdfService;

        public PdfController(IPdfService pdfService)
        {
            _pdfService = pdfService;
        }

        [HttpPost]
        public IActionResult GetPdf(Review review)
        {
            var result = _pdfService.GetReviewByteArray(review);
            var res = Convert.ToBase64String(result);
            return Ok(res);
        }
    }
}