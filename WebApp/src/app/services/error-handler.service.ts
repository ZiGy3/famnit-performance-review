import {Injectable} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {HttpErrorResponse} from '@angular/common/http';
import {Observable, Subject, throwError} from 'rxjs';
import {RuleViolation} from '../infrastructure/error-info';
import {ErrorModalComponent} from '../components/error-modal/error-modal.component';
import {User} from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService {

  private unauthorizedSource: Subject<any>;
  public unauthorizedStream: Observable<User>;

  constructor(private modalService: NgbModal) {
    this.unauthorizedSource = new Subject();
    this.unauthorizedStream = this.unauthorizedSource.asObservable();
  }

  handleHttpError = (error) => {
    if (error instanceof HttpErrorResponse) {
      if (error.status === 401) {
        this.unauthorizedSource.next();
      } else if (error.status === 400) {
        if (error.error.ValidationErrors && error.error.ValidationErrors.length > 0) {
          this.openErrorModal('Validation Error', error.error.Message);
        }
      } else if (error.status === 500) {
        this.openErrorModal('Server Error', error.error.Message);
      } else {
        this.openErrorModal('Server Error', error.message);
      }
    }
    return throwError(error);
  }

  private openErrorModal(title: string, message: string) {
    const modalRef = this.modalService.open(ErrorModalComponent);
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.message = message;
  }
}
