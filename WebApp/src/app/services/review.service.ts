import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Review, ReviewStatus} from '../models/review';
import {catchError} from 'rxjs/operators';
import {ErrorHandlerService} from './error-handler.service';
import {RatingScale} from '../models/ratingScale';
import {Tuple} from '../infrastructure/tuple';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  constructor(private http: HttpClient, private errorService: ErrorHandlerService) { }

  private readonly rootUrl = 'api/review';

  private readonly httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  getReview(year: number): Observable<Review> {
    return this.http.get<Review>(`${this.rootUrl}/${year}`).pipe(
      catchError(this.errorService.handleHttpError)
    );
  }

  getEmployeeReview(year: number, userGuid: string): Observable<Review> {
    return this.http.get<Review>(`${this.rootUrl}/employeeReview/${year}/${userGuid}`).pipe(
      catchError(this.errorService.handleHttpError)
    );
  }

  changeStatus(reviewId: number, status: ReviewStatus): Observable<any> {
    return this.http.post(this.rootUrl + '/changeStatus', { reviewId, status }, this.httpOptions).pipe(
      catchError(this.errorService.handleHttpError)
    );
  }

  addFinalAssessment(reviewId: number, rating: RatingScale): Observable<any> {
    return this.http.post(this.rootUrl + '/finalAssessment', { reviewId, rating }, this.httpOptions).pipe(
      catchError(this.errorService.handleHttpError)
    );
  }

  getEmployeesReviewStatus(year: number): Observable<Tuple<string, ReviewStatus>[]> {
    return this.http.get<any[]>(`${this.rootUrl}/employeesReviewStatus/${year}`).pipe(
      catchError(this.errorService.handleHttpError)
    );
  }
}
