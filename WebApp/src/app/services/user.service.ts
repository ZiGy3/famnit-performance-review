import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Login } from '../models/login';
import { Observable, BehaviorSubject } from 'rxjs';
import { User } from '../models/user';
import { ErrorHandlerService } from './error-handler.service';
import { catchError, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ProgressService } from './progress.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private rootUrl = 'api/user';
  private userSource: BehaviorSubject<User>;
  public userStream: Observable<User>;

  private readonly httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient, private errorService: ErrorHandlerService, private router: Router, private progressService: ProgressService) {
    this.userSource = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.userStream = this.userSource.asObservable();

    if (!this.currentUser) {
      this.router.navigate(['/login']).then(() => null);
    }

    errorService.unauthorizedStream.subscribe(() => this.logout());
  }

  get currentUser(): User {
    return this.userSource.value;
  }

  login(login: Login): Observable<User> {
    return this.http.post<User>(this.rootUrl + '/authenticate', login, this.httpOptions).pipe(
      tap(user => {
        if (user && user.token) {
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.userSource.next(user);
        }
      })
    );
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.userSource.next(null);
    this.router.navigate(['/login']).then(() => null);
    this.progressService.stopProgress();
  }

  getEmployeeList(): Observable<User[]> {
    return this.http.get<User[]>(`${this.rootUrl}/employeeList/`).pipe(
      catchError(this.errorService.handleHttpError)
    );
  }
}
