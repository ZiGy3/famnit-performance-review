import {Injectable} from '@angular/core';
import {HttpHeaders, HttpClient} from '@angular/common/http';
import {ErrorHandlerService} from './error-handler.service';
import {Observable} from 'rxjs';
import {Goal} from '../models/goal';
import {catchError} from 'rxjs/operators';
import {RatingScale} from '../models/ratingScale';

@Injectable({
  providedIn: 'root'
})
export class GoalService {

  private readonly rootUrl = 'api/goal';

  private readonly httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient, private errorService: ErrorHandlerService) {
  }

  getGoalList(reviewId: number): Observable<Goal[]> {
    return this.http.get<Goal[]>(`${this.rootUrl}/list/${reviewId}`).pipe(
      catchError(this.errorService.handleHttpError)
    );
  }

  addGoal(goal: Goal): Observable<Goal> {
    return this.http.post<Goal>(this.rootUrl, goal, this.httpOptions).pipe(
      catchError(this.errorService.handleHttpError)
    );
  }

  updateGoal(goal: Goal): Observable<Goal> {
    return this.http.put<Goal>(this.rootUrl, goal, this.httpOptions).pipe(
      catchError(this.errorService.handleHttpError)
    );
  }

  deleteGoal(goalId: number): Observable<any> {
    return this.http.delete<any>(`${this.rootUrl}/${goalId}`).pipe(
      catchError(this.errorService.handleHttpError)
    );
  }

  addEmployeeAssessment(goalId: number, rating: RatingScale): Observable<any> {
    return this.http.post(this.rootUrl + '/employeeAssessment', {goalId, rating}, this.httpOptions).pipe(
      catchError(this.errorService.handleHttpError)
    );
  }

  addSuperiorAssessment(goalId: number, rating: RatingScale): Observable<any> {
    return this.http.post(this.rootUrl + '/superiorAssessment', {goalId, rating}, this.httpOptions).pipe(
      catchError(this.errorService.handleHttpError)
    );
  }
}
