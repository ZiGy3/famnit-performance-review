import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VersionService {

  private rootUrl = 'api/version';

  private readonly httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  getVersion(): Observable<any> {
    return this.http.get<any>(this.rootUrl, this.httpOptions).pipe();
  }
}
