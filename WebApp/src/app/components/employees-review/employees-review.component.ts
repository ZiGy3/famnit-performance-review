import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from 'src/app/services/user.service';
import {forkJoin, Subject, Subscription} from 'rxjs';
import {User} from 'src/app/models/user';
import {ReviewService} from 'src/app/services/review.service';
import {ContextService} from 'src/app/services/context.service';
import {Tuple} from 'src/app/infrastructure/tuple';
import {ReviewStatus} from 'src/app/models/review';

@Component({
  selector: 'pr-employees-review',
  templateUrl: './employees-review.component.html',
  styleUrls: ['./employees-review.component.scss']
})
export class EmployeesReviewComponent implements OnInit, OnDestroy {
  private employeeList: User[];
  employeeListDisplay: User[];
  reviewStatusList: Tuple<string, ReviewStatus>[];
  employeeSource: Subject<User> = new Subject<User>();
  employeeListSourceSubscription: Subscription;
  employeeSourceSubscription: Subscription;
  contextChangeSubscription: Subscription;
  selectedEmployee = null;

  constructor(private userService: UserService, private reviewService: ReviewService, private contextService: ContextService) { }

  ngOnInit() {
    this.loadData();
    this.employeeSourceSubscription = this.employeeSource.subscribe(user => {
      this.selectedEmployee = user;
    });
    this.contextChangeSubscription = this.contextService.contextChangeStream.subscribe(() => {
      this.refreshStatus();
    });
  }

  ngOnDestroy(): void {
    if (this.employeeListSourceSubscription) {
      this.employeeListSourceSubscription.unsubscribe();
    }
    if (this.employeeSourceSubscription) {
      this.employeeSourceSubscription.unsubscribe();
    }
    if (this.contextChangeSubscription) {
      this.contextChangeSubscription.unsubscribe();
    }
  }

  loadData() {
    forkJoin([this.userService.getEmployeeList(), this.reviewService.getEmployeesReviewStatus(this.contextService.year)]).subscribe(results => {
      this.employeeList = results[0];
      this.employeeListDisplay = results[0];
      this.reviewStatusList = results[1];
      if (this.employeeList.length > 0) {
        this.employeeSource.next(this.employeeList[0]);
      }
    });
  }

  refreshStatus() {
    this.reviewService.getEmployeesReviewStatus(this.contextService.year).subscribe(statusList => {
      this.reviewStatusList = statusList;
    });
  }

  employeeSelect(employee: User) {
    this.employeeSource.next(employee);
  }

  onSearchChange(term: string) {
    this.employeeListDisplay = this.employeeList.filter(user => user.displayName.toLowerCase().includes(term.toLowerCase()));
  }
}
