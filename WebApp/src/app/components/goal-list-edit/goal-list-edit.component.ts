import {Component, Input, OnInit, QueryList, ViewChildren} from '@angular/core';
import {Review, ReviewStatus} from 'src/app/models/review';
import {GoalService} from 'src/app/services/goal.service';
import {Goal, GoalType} from 'src/app/models/goal';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GoalEditComponent} from '../goal-edit/goal-edit.component';

@Component({
  selector: 'pr-goal-list-edit',
  templateUrl: './goal-list-edit.component.html',
  styleUrls: ['./goal-list-edit.component.scss']
})
export class GoalListEditComponent implements OnInit {
  public review: Review;

  @Input()
  set reviewInput(value: Review) {
    this.review = value;
    this.loadData();
  }

  @Input() superiorView = false;
  @ViewChildren(GoalEditComponent) goalEditComponents: QueryList<GoalEditComponent>;
  reviewStatus = ReviewStatus;
  goalList: Goal[];
  modalAddGoal: NgbModalRef;
  addGoalForm: FormGroup;

  constructor(private goalService: GoalService, private modalService: NgbModal, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.addGoalForm = this.formBuilder.group({
      title: this.formBuilder.control(null, Validators.required),
      description: this.formBuilder.control(null, Validators.required)
    });
  }

  get controls() {
    return this.addGoalForm.controls;
  }

  loadData() {
    this.goalService.getGoalList(this.review.id).subscribe(data => this.goalList = data);
  }

  openAddGoalModal(content) {
    this.addGoalForm.reset();
    this.modalAddGoal = this.modalService.open(content, {backdrop: 'static', size: 'lg'});
  }

  addGoal() {
    if (this.addGoalForm.invalid) {
      return;
    }
    const goal = this.addGoalForm.value as Goal;
    goal.reviewId = this.review.id;
    goal.type = GoalType.Custom;
    this.goalService.addGoal(goal).subscribe(() => {
      this.loadData();
      this.modalAddGoal.close();
    });
  }

  deleteGoal() {
    this.loadData();
  }

  updateGoal() {
    this.loadData();
  }

  updateComment() {
    this.loadData();
  }

  deleteComment() {
    this.loadData();
  }

  validate(newStatus: ReviewStatus): string[] {
    const error = [];
    if (newStatus === ReviewStatus.CustomGoalsDefined) {
      if (this.goalList.length < 3) {
        error.push('Add at least 3 custom goals');
      }
    }
    if ((newStatus === ReviewStatus.EmployeeAssessment) || (newStatus === ReviewStatus.SuperiorAssessment)) {
      if (this.goalEditComponents.map(x => x.isValid()).filter(x => x === false).length > 0) {
        error.push('Validation error in goals');
      }
    }
    return error;
  }
}
