import {Component, Input, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'pr-panel',
  templateUrl: 'panel.component.html',
  styleUrls: ['panel.component.scss'],
  animations: [
    trigger('panelState', [
      state('expanded', style({height: '*'})),
      state('collapsed', style({height: '0px', display: 'none'})),
      transition('expanded <=> collapsed', animate('200ms'))
    ]),
    trigger('arrowState', [
      state('expanded', style({transform: 'rotate(90deg)'})),
      state('collapsed', style({})),
      transition('expanded <=> collapsed', animate('200ms'))
    ])
  ]
})

export class PanelComponent implements OnInit {


  @Input() headerTitle: string;
  @Input() expansive = false;
  @Input() autoExpand = false;
  classList: string[];
  isExpanded: boolean;

  constructor() {
  }

  ngOnInit() {
    this.classList = this.classList ? this.classList : [];
    if (this.expansive) {
      this.classList.push('expansive');
    }
    this.isExpanded = !(this.expansive && !this.autoExpand);
  }
}
