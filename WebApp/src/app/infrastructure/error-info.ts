export interface RuleViolation {
    Identifier: string;
    Message: string;
    IsWarning: boolean;
}
