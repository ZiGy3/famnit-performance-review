export class User {
    guid: string;
    displayName: string;
    token: string;
    isSuperior: boolean;
}
