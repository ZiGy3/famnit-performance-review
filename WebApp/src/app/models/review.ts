import {RatingScale} from './ratingScale';
import {Comment} from './comment';

export class Review {
  id: number;
  status: ReviewStatus;
  year: number;
  comments: Comment[];
  finalAssessment?: RatingScale;
}


export enum ReviewStatus {
  PendingGoalsDefinition = 1,
  CustomGoalsDefined = 2,
  SuperiorGoalsConfirmed = 3,
  EmployeeAssessment = 4,
  SuperiorAssessment = 5,
  ManagementAssessment = 6,
  NoReview = 7
}
