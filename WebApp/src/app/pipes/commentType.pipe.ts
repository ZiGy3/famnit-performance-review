import { Pipe, PipeTransform } from '@angular/core';
import { CommentType } from '../models/comment';

@Pipe({ name: 'commentType' })
export class CommentTypePipe implements PipeTransform {

    transform(value: CommentType): string {
        switch (value) {
            case CommentType.GoalsReject:
                return '';
            case CommentType.EmployeeAssessment:
                return 'Employee rate';
            case CommentType.SuperiorAssessment:
                return 'Superior rate';
            case CommentType.EmployeeFinal:
                return 'Employee final';
            case CommentType.SuperiorFinal:
                return 'Superior final';
        }
    }
}
