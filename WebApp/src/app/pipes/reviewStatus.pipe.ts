import {Pipe, PipeTransform} from '@angular/core';
import {Tuple} from '../infrastructure/tuple';
import {ReviewStatus} from '../models/review';
import {DomSanitizer} from '@angular/platform-browser';

@Pipe({name: 'reviewStatus'})
export class ReviewStatusPipe implements PipeTransform {

  constructor(private sanitized: DomSanitizer) {
  }

  transform(value: string, statusList: Tuple<string, ReviewStatus>[]) {
    const status = statusList.find(x => x.item1 === value);
    if (status) {
      switch (status.item2) {
        case ReviewStatus.PendingGoalsDefinition:
          return this.sanitized.bypassSecurityTrustHtml('<span class="font-weight-bold">1</span>');
        case ReviewStatus.CustomGoalsDefined:
          return this.sanitized.bypassSecurityTrustHtml('<span class="fa fa-exclamation fa-lg mt-1 mr-1" style="color:#dc3545"></span><span class="font-weight-bold">2</span>');
        case ReviewStatus.SuperiorGoalsConfirmed:
          return this.sanitized.bypassSecurityTrustHtml('<span class="font-weight-bold">3</span>');
        case ReviewStatus.EmployeeAssessment:
          return this.sanitized.bypassSecurityTrustHtml('<span class="fa fa-exclamation fa-lg mt-1 mr-1" style="color:#dc3545"></span><span class="font-weight-bold">4</span>');
        case ReviewStatus.SuperiorAssessment:
          return this.sanitized.bypassSecurityTrustHtml('<span class="fa fa-exclamation fa-lg mt-1 mr-1" style="color:#dc3545"></span><span class="font-weight-bold">5</span>');
        case ReviewStatus.ManagementAssessment:
          return this.sanitized.bypassSecurityTrustHtml('<span class="fa fa-check fa-lg mt-1 mr-1" style="color:green"></span><span class="font-weight-bold">6</span>');
        case ReviewStatus.NoReview:
          return this.sanitized.bypassSecurityTrustHtml('<span class="fa fa-check fa-lg mt-1 mr-1" style="color:green"></span><span class="font-weight-bold">N-R</span>');
      }
    }
    return '';
  }
}
